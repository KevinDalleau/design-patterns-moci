package eu.telecomnancy.ui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.*;

public class MainWindowState extends JFrame{
	private SensorStateConcrete sensor;
    private SensorView sensorView;

    public MainWindowState(SensorStateConcrete sensor) {
        this.sensor = sensor;
        this.sensorView = new SensorView(this.sensor);
        sensor.addObserver(sensorView);
        this.setLayout(new BorderLayout());
        this.add(this.sensorView, BorderLayout.CENTER);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }

}
