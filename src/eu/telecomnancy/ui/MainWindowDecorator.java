package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.*;

import javax.swing.*;
import java.awt.*;

public class MainWindowDecorator extends JFrame {

    private SensorDecorator sensor;
    private SensorViewDecorator sensorViewDecorator;

    public MainWindowDecorator(SensorDecorator sensor) {
        this.sensor = sensor;
        this.sensorViewDecorator = new SensorViewDecorator(this.sensor);
        ((SensorDecorator) sensor).addObserver(sensorViewDecorator);
        this.setLayout(new BorderLayout());
        this.add(this.sensorViewDecorator, BorderLayout.CENTER);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }


}
