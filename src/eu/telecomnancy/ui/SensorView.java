package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TurnSensorOff;
import eu.telecomnancy.sensor.TurnSensorOn;
import eu.telecomnancy.sensor.UpdateCommand;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class SensorView extends JPanel implements Observer {
    private ISensor sensor;

    private JLabel value = new JLabel("N/A ��C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
   
    public SensorView(ISensor c) {
        this.sensor = c;
        this.setLayout(new BorderLayout());
        
        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	TurnSensorOn commandSensor = new TurnSensorOn(sensor);
                commandSensor.execute();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	TurnSensorOff commandSensor = new TurnSensorOff(sensor);
                commandSensor.execute();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UpdateCommand commandSensor = new UpdateCommand(sensor);
				commandSensor.execute();
            }
        });
        
        

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 4));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);
        
        this.add(buttonsPanel, BorderLayout.SOUTH);
    }
    
	@Override
	public void update(Observable o, Object arg) {
		value.setText(arg.toString() + " ° Celcius");
		this.repaint();
		System.out.println(value.getText());
		
	}
}
