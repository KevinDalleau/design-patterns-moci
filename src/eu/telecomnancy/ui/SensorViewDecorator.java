package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorDecorator;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class SensorViewDecorator extends JPanel implements Observer {
    private SensorDecorator sensor;

    private JLabel value = new JLabel("N/A");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    private JButton unit = new JButton("Switch unit");

    public SensorViewDecorator(SensorDecorator c) {
        this.sensor = c;
        
        this.setLayout(new BorderLayout());
        
        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sensor.on();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sensor.off();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    sensor.update();
                    
                    } catch (SensorNotActivatedException sensorNotActivatedException) {
                    sensorNotActivatedException.printStackTrace();
                }
            }
        });
        
        unit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(sensor.getUnit()=='C') {
                	sensor.switchToFarenheit();
                	System.out.println(sensor.getClass());
                }
                if(sensor.getUnit()=='F') {
                	sensor.switchToCelcius();
                	System.out.println(sensor.getClass());
                	
                }
                    
                    
                   
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 4));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);
        buttonsPanel.add(unit);
        this.add(buttonsPanel, BorderLayout.SOUTH);
    }
    
	@Override
	public void update(Observable o, Object arg) {
		value.setText(arg.toString());
		this.repaint();
		System.out.println(value.getText());
		
	}
}
