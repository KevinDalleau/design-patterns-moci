package eu.telecomnancy;

import eu.telecomnancy.sensor.SensorDecorator;
import eu.telecomnancy.sensor.TemperatureSensorCelcius;
import eu.telecomnancy.sensor.TemperatureSensorDecorator;
import eu.telecomnancy.sensor.TemperatureSensorFarenheit;
import eu.telecomnancy.ui.MainWindowDecorator;

public class AppDecorator {
	public static void main(String[] args) {
        SensorDecorator sensor = new TemperatureSensorFarenheit(new TemperatureSensorDecorator());
        new MainWindowDecorator(sensor);
    }
}
