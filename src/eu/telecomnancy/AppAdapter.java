package eu.telecomnancy;

import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.SensorAdapter;
import eu.telecomnancy.ui.ConsoleUI;

public class AppAdapter {

	
	public static void main(String[] args) {
		SensorAdapter adapter = new SensorAdapter(new LegacyTemperatureSensor());
		new ConsoleUI(adapter);
	}

}
