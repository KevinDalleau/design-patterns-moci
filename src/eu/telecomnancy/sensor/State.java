package eu.telecomnancy.sensor;

public interface State{
	
	
	public boolean getStatus();
	public void update();
	public double getValue();
	public void setValue(Double value);
	public double updateState();
	
}
