package eu.telecomnancy.sensor;

import java.util.Random;

public class TemperatureSensorCelcius extends SensorDecorator{
	
	String value;
	char unit;
	public TemperatureSensorCelcius(ISensor sensor) {
		super(sensor);
		this.unit = 'C';
	}
	 	@Override
	    public void on() {
	        unitSensor.on();
	        //setData();
	    }

	    @Override
	    public void off() {
	        unitSensor.off();
	        //setData();
	    }

	    @Override
	    public boolean getStatus() {
	        return unitSensor.getStatus();
	    }
	    
	    @Override
	    public void update() throws SensorNotActivatedException {
	        if (unitSensor.getStatus()) {
	            value = Double.toString(unitSensor.getValue()) + "°C";
	            setChanged();
	        	notifyObservers(value);	    		      	
	        	
	        }
	        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	    }


		public double getValue() throws SensorNotActivatedException {
		
		return unitSensor.getValue();
	}
		public char getUnit() {
			return unit;
		}
		
}
