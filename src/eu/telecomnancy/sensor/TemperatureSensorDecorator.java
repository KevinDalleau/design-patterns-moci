package eu.telecomnancy.sensor;

import java.util.Observable;
import java.util.Random;


public class TemperatureSensorDecorator extends Observable implements ISensor {
    boolean state;
    double value;
    char unit;
    @Override
    public void on() {
        state = true;
        //setData();
    }

    @Override
    public void off() {
        state = false;
        //setData();
    }

    @Override
    public boolean getStatus() {
        return state;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (state) {
           

        }
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
    	value = (new Random()).nextDouble() * 100;
        if (state)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }
    
    public char getUnit() {
    	return this.getUnit();
    }
}
