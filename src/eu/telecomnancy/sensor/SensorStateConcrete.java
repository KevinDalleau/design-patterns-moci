package eu.telecomnancy.sensor;

import java.util.Observable;

public class SensorStateConcrete extends Observable implements ISensor{
 State on;
 State off;
 State concreteState;
 double value = 0;
 
public SensorStateConcrete() {
	on = new SensorStateOn(this);
	off = new SensorStateOff(this);
}

public void setState(State state) {
	this.concreteState= state;
}

public boolean getStatus() {
	return concreteState.getStatus();
}

@Override
public void update() {
	value = concreteState.updateState();
	setChanged();
	notifyObservers(value);
}

@Override
public void on() {
	this.setState(new SensorStateOn(this));
	
}

@Override
public void off() {
	this.setState(new SensorStateOff(this));
	
}

@Override
public double getValue() {
	
	return concreteState.getValue();
}


}
