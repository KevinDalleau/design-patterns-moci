package eu.telecomnancy.sensor;

public class UpdateCommand implements SensorCommand{
	ISensor sensor;
	
	public UpdateCommand(ISensor sensor) {
		this.sensor = sensor;
	}

	@Override
	public void execute() {
		try {
			sensor.update();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
}
