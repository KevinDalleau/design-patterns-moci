package eu.telecomnancy.sensor;

import java.util.Observable;
import java.util.Random;

public class SensorStateOff extends Observable implements State {
	SensorStateConcrete state;
	double value = 0;
	
	public SensorStateOff(SensorStateConcrete state) {
		this.state = state;
	}
	@Override
	public boolean getStatus() {
		 return false;
    }

    @Override
    public void update() {
           System.out.println("Ceci n'est pas possible, le capteur n'est pas allumé");
        	
        	
        }
       
  

    @Override
    public double getValue() {
       
            return state.value;
        
        
    }

	@Override
	public void setValue(Double value) {
		state.value = value;
		
	}
	@Override
	public double updateState() {
		
		return 0;
	}
    
    
}
