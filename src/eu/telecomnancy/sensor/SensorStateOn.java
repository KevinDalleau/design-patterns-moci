package eu.telecomnancy.sensor;

import java.util.Observable;
import java.util.Random;

public class SensorStateOn extends Observable implements State {
	SensorStateConcrete state;
	double value = 0;
	public SensorStateOn(SensorStateConcrete state) {
		this.state = state;
	}
	
	
    
	
	@Override
	public boolean getStatus() {
		 return true;
    }
	@Override
    public double updateState()  {
    	value = (new Random()).nextDouble() * 100;
    	return value;
//        setChanged();
//    	notifyObservers(value);
		
    }
       
  

    @Override
    public double getValue() {
       
            return state.value;
        
        
    }

	@Override
	public void setValue(Double value) {
		state.value = value;
		
	}




	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}




}
