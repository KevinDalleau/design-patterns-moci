package eu.telecomnancy.sensor;

import java.util.Random;

public class TemperatureSensorFarenheit extends SensorDecorator{
	
	String value;
	char unit;
	public TemperatureSensorFarenheit(ISensor sensor) {
		super(sensor);
		this.unit='F';
	}

	@Override
    public void on() {
        unitSensor.on();
        //setData();
    }

    @Override
    public void off() {
        unitSensor.off();
        //setData();
    }

    @Override
    public boolean getStatus() {
        return unitSensor.getStatus();
    }
    
    
    @Override
    public void update() throws SensorNotActivatedException {
        if (unitSensor.getStatus()) {
            value = Double.toString((new Random()).nextDouble() * 100 * 9/5 + 32) + " °F";
            setChanged();
        	notifyObservers(value);
    		
        }
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }


	public double getValue() throws SensorNotActivatedException {
	
	return unitSensor.getValue();
}
	public char getUnit() {
		return this.unit;
	}
	
}
