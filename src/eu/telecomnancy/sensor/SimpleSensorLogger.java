package eu.telecomnancy.sensor;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 13/12/13
 * Time: 18:16
 */
public class SimpleSensorLogger implements SensorLogger {
    @Override
    public void log(LogLevel level, Date date, String message) {
        System.out.println(level.name() + " "+ date + " " + message);
    }
}
