package eu.telecomnancy.sensor;

public class SensorAdapter implements ISensor {
	
	private LegacyTemperatureSensor LegacyTemperatureSensor;
	
	public SensorAdapter(LegacyTemperatureSensor adaptee) {
		
		
		this.LegacyTemperatureSensor = adaptee;
		System.out.println("Un nouveau Legacy Temperature Sensor a été créé");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void on() {
		if(!this.LegacyTemperatureSensor.getStatus()) {
			this.LegacyTemperatureSensor.onOff();
		}
		
	}

	@Override
	public void off() {
		if(this.LegacyTemperatureSensor.getStatus()) {
			this.LegacyTemperatureSensor.onOff();
		}
		
	}

	@Override
	public boolean getStatus() {
		
		return this.LegacyTemperatureSensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		return;
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		 if (this.LegacyTemperatureSensor.getStatus())
	            return this.LegacyTemperatureSensor.getTemperature();
	        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}

}
