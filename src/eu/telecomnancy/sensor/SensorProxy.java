package eu.telecomnancy.sensor;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 13/12/13
 * Time: 18:07
 */
public class SensorProxy implements ISensor {
    private ISensor sensor;
    private SensorLogger log;
    private Date date;
    public SensorProxy(ISensor _sensor, SensorLogger sensorLogger) {
        sensor = _sensor;
        log = sensorLogger;
    }

    @Override
    public void on() {
    	Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
    	date = new Date(timeStamp.getTime());
        log.log(LogLevel.INFO, date, " Sensor On");
        sensor.on();
    }

    @Override
    public void off() {
    	Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
    	date = new Date(timeStamp.getTime());
        log.log(LogLevel.INFO, date," Sensor Off");
        sensor.off();
    }

    @Override
    public boolean getStatus() {
    	Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
    	date = new Date(timeStamp.getTime());
        log.log(LogLevel.INFO, date, " "+"Sensor getStatus = "+sensor.getStatus());
        return sensor.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
    	Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
    	date = new Date(timeStamp.getTime());
        log.log(LogLevel.INFO, date, " Sensor update");
        sensor.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
    	Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
    	Date date = new Date(timeStamp.getTime());
        log.log(LogLevel.INFO, date, " Sensor value =" + sensor.getValue());
        return sensor.getValue();
    }
}
