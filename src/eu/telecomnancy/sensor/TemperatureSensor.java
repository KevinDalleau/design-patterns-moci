package eu.telecomnancy.sensor;

import java.util.Observable;
import java.util.Random;

import eu.telecomnancy.ui.SensorView;

public class TemperatureSensor extends Observable implements ISensor {
    boolean state;
    double value = 0;
    
    @Override
    public void on() {
        state = true;
        //setData();
    }

    @Override
    public void off() {
        state = false;
        //setData();
    }

    @Override
    public boolean getStatus() {
        return state;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (state) {
            value = (new Random()).nextDouble() * 100;
            setChanged();
        	try {
    			notifyObservers(this.getValue());
    		} catch (SensorNotActivatedException e) {
    			
    			e.printStackTrace();
    		}
        	
        	
        }
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (state)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }
    
    
}
