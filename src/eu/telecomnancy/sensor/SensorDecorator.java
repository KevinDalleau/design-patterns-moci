package eu.telecomnancy.sensor;

import java.util.Observable;

import eu.telecomnancy.ui.MainWindowDecorator;

public abstract class SensorDecorator extends Observable implements ISensor{
protected ISensor unitSensor;
public char unit;

	public SensorDecorator(ISensor sensor) {
		unitSensor = sensor;
	}
	@Override
	public void on() {
		unitSensor.on();
		
	}
	@Override
	public void off() {
		unitSensor.off();
		
	}
	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		return unitSensor.getStatus();
	}
	@Override
	public void update() throws SensorNotActivatedException {
		unitSensor.update();
		
	}
	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		return unitSensor.getValue();
	}
	
	public char getUnit() {
		return this.getUnit();
		
	}
	
	public void switchToFarenheit() {
		unitSensor = new TemperatureSensorFarenheit(new TemperatureSensorDecorator());
		new MainWindowDecorator(new TemperatureSensorFarenheit(new TemperatureSensorDecorator()));
		try {
			unitSensor.on();
			unitSensor.update();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void switchToCelcius() {
		unitSensor = new TemperatureSensorCelcius(new TemperatureSensorDecorator());
		new MainWindowDecorator(new TemperatureSensorCelcius(new TemperatureSensorDecorator()));
		try {
			unitSensor.on();
			unitSensor.update();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
