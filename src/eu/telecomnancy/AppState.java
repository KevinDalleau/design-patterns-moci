package eu.telecomnancy;

import eu.telecomnancy.sensor.SensorStateConcrete;
import eu.telecomnancy.sensor.SensorStateOn;
import eu.telecomnancy.ui.MainWindowState;

public class AppState {
	public static void main(String[] args) {
        SensorStateConcrete sensor = new SensorStateConcrete();
        sensor.setState(new SensorStateOn(sensor)); //Par défaut, le capteur est allumé
        new MainWindowState(sensor);
    }
}
